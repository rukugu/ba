#   Backend Mini Project (Bank account)

-   To run this project, just open it in IntelliJ and run the BankAccountApplication 
    configuration.
-   To run the tests, select the BankaccountApplicationTests configuration and run. 
    Make sure to run the BankAccountApplication first 
-   If using another IDE, just import and configure.
-   Code coverage results can viewed in IntelliJ's Coverage window, or viewed 
    in repo's folder. This code uses RxJava, hence the coverage results.
    Just select 'Run with Coverage' from the Run menu
-   If you're inclined to test with Postman, 
    find the exported API data in postman_collection.json file.
-   App used H2 in-memory Db which will lose data when application is stopped.
