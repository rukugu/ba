package co.tala.bankaccount;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.net.URI;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = BankaccountApplication.class)
@WebAppConfiguration
@EnableWebMvc
@AutoConfigureMockMvc
public class BankaccountApplicationTests {

    private static final long ACCOUNT_ID = 1L;

    @Before
    public void setup() {

    }

    @Test
    public void getCurrentBalanceTest() {
        RestTemplate restTemplate = new RestTemplate();
        assertThat(restTemplate.getForObject(String.format("http://localhost:8080/balance/%d", ACCOUNT_ID), String.class)).contains("$");
    }

    @Test(expected = HttpClientErrorException.class)
    public void depositMoreThanSingleAmountAllowedTest() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"depositAmount\":41000.00}", headers);
        restTemplate.postForEntity(URI.create("http://localhost:8080/deposit"), entity, Exception.class);
    }

    @Test(expected = HttpClientErrorException.class)
    public void depositMoreThanMaxAllowedDailyAmountTest() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"depositAmount\":151000.00}", headers);
        restTemplate.postForEntity(URI.create("http://localhost:8080/deposit"), entity, Exception.class);
    }

    @Test(expected = HttpClientErrorException.class)
    public void depositMoreThanMaxAllowedDailyTransactionsTest() {
        for (int i = 0; i < 5; ++i) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"depositAmount\":100.00}", headers);
            restTemplate.postForObject(URI.create("http://localhost:8080/deposit"), entity, String.class);
        }
    }


    @Test(expected = HttpClientErrorException.class)
    public void withdrawMoreThanMaxAllowedDailyAmountTest() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"withdrawalAmount\":51000.00}", headers);
        restTemplate.postForEntity(URI.create("http://localhost:8080/withdraw"), entity, Exception.class);
    }


    @Test(expected = HttpClientErrorException.class)
    public void withdrawMoreThanSingleAmountAllowedTest() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"withdrawalAmount\":21000.00}", headers);
        restTemplate.postForEntity(URI.create("http://localhost:8080/withdraw"), entity, Exception.class);
    }

    @Test(expected = HttpClientErrorException.class)
    public void withdrawMoreThanMaxAllowedDailyTransactionsTest() {
        for (int i = 0; i < 5; ++i) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>("{\"accountId\":1,\"withdrawalAmount\":100.00}", headers);
            restTemplate.postForObject(URI.create("http://localhost:8080/withdraw"), entity, String.class);
        }
    }

    @Test(expected = HttpClientErrorException.class)
    public void withdrawMoreThanBalanceTest() {
        RestTemplate restTemplate = new RestTemplate();
        String balance = (restTemplate.getForObject(String.format("http://localhost:8080/balance/%d", ACCOUNT_ID), String.class));
        Double withdrawalAmount = Double.valueOf(balance.split(" ")[0]);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(String.format("{\"accountId\":1,\"withdrawalAmount\":%s}", withdrawalAmount + 1), headers);
        restTemplate.postForObject(URI.create("http://localhost:8080/withdraw"), entity, String.class);
    }

    @After
    public void tearDown() {

    }


}


