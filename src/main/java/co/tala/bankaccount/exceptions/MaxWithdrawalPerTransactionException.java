package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "You have entered an amount larger than the limit for a single withdrawal. Please select a lesser amount")
public class MaxWithdrawalPerTransactionException extends Exception {
}
