package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT, reason = "You have entered an amount larger than the limit for a single deposit")
public class MaxDepositPerTransactionException extends Exception {
}
