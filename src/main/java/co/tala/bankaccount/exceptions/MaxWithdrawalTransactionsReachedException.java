package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "You have reached the maximum number of withdrawal transactions you are allowed daily")
public class MaxWithdrawalTransactionsReachedException extends Exception {
}
