package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "The amount you want to withdraw is more than your account balance")
public class BalanceLessThanWithdrawalException extends Exception {

}
