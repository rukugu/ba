package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "You have reached the maximum deposit amount allowed today")
public class MaxDailyDepositException extends Exception {
}
