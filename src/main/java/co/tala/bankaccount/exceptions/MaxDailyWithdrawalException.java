package co.tala.bankaccount.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Dennis Mwangi
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "You will exceed the maximum withdrawal amount allowed today. Please select a lesser amount")
public class MaxDailyWithdrawalException extends Exception {
}
