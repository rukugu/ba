package co.tala.bankaccount.utils;

import java.math.BigDecimal;

/**
 * @author Dennis Mwangi
 */

public final class Constants {

    public static final BigDecimal MAX_DAILY_DEPOSIT = BigDecimal.valueOf(150_000);

    public static final BigDecimal MAX_DEPOSIT_PER_TRANSACTION = BigDecimal.valueOf(40_000);

    public static final int MAX_DEPOSIT_TRANSACTIONS_DAILY = 4;

    public static final BigDecimal MAX_DAILY_WITHDRAWAL = BigDecimal.valueOf(50_000);

    public static final BigDecimal MAX_WITHDRAWAL_PER_TRANSACTION = BigDecimal.valueOf(20_000);

    public static final int MAX_WITHDRAWAL_TRANSACTIONS_DAILY = 3;
}
