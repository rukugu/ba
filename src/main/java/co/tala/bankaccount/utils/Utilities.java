package co.tala.bankaccount.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author Dennis Mwangi
 */
public final class Utilities {

    private final static String PATTERN = "yyyy-MM-dd";

    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern(PATTERN);

    public static String dateToday() {
        DateTime now = new org.joda.time.DateTime();
        return DATE_TIME_FORMATTER.print(now);
    }
}
