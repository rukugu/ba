package co.tala.bankaccount.models;


import javax.persistence.*;

/**
 * @author Dennis Mwangi
 */
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, name = "accountId")
    private Long accountId;

    @Column(nullable = false, name = "userId")
    private Long userId;

    public Account() {

    }
}
