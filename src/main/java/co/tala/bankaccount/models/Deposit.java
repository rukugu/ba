package co.tala.bankaccount.models;


import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Dennis Mwangi
 */

@Entity
@Table(name = "deposits")
public class Deposit {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, name = "accountId")
    private Long accountId;

    @Column(nullable = false, name = "depositAmount")
    private String depositAmount;

    @Column(nullable = false, columnDefinition = "DATE DEFAULT CURRENT_DATE", name = "timestamp", insertable = false, updatable = false)
    private String timeStamp;

    public Deposit() {

    }

    public Deposit(Long id, String amount) {
        this.accountId = id;
        this.depositAmount = amount;
    }

    @NotNull
    public BigDecimal getDepositAmout() {
        return BigDecimal.valueOf(Double.valueOf(this.depositAmount));
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public Long getAccountId() {
        return this.accountId;
    }
}
