package co.tala.bankaccount.models;


import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Dennis Mwangi
 */
@Entity
@Table(name = "withdrawals")
public class Withdrawal {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, name = "accountId")
    private Long accountId;

    @Column(nullable = false, name = "withdrawalAmount")
    private String withdrawalAmount;

    @Column(nullable = false, columnDefinition = "DATE DEFAULT CURRENT_DATE", name = "timestamp", insertable = false, updatable = false)
    private String timeStamp;

    public Withdrawal() {

    }

    public Withdrawal(Long id, String amount) {
        this.accountId = id;
        this.withdrawalAmount = amount;
    }

    @NotNull
    public BigDecimal getWithdrawalAmount() {
        return BigDecimal.valueOf(Double.valueOf(this.withdrawalAmount));
    }

    public Long getAccountId() {
        return this.accountId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
