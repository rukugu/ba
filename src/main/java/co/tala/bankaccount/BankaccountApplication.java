package co.tala.bankaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Dennis Mwangi
 */
@SpringBootApplication
@EnableJpaRepositories("co.tala.bankaccount.*")
@ComponentScan(basePackages = {"co.tala.bankaccount.*"})
@EntityScan("co.tala.bankaccount.*")
@EnableAutoConfiguration
public class BankaccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankaccountApplication.class, args);
    }
}
