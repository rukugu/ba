package co.tala.bankaccount.controllers;

import co.tala.bankaccount.data.AccountRepository;
import co.tala.bankaccount.data.DepositRepository;
import co.tala.bankaccount.data.WithdrawalRepository;
import co.tala.bankaccount.exceptions.*;
import co.tala.bankaccount.models.Deposit;
import co.tala.bankaccount.models.Withdrawal;
import co.tala.bankaccount.utils.Constants;
import co.tala.bankaccount.utils.Utilities;
import com.google.gson.Gson;
import io.reactivex.Single;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Dennis Mwangi
 */
@RestController
public class AccountController {

    private static final Gson GSON = new Gson();

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private WithdrawalRepository withdrawalRepository;

    @Autowired
    private DepositRepository depositRepository;

    @RequestMapping(value = "/balance/{accountId}", method = RequestMethod.GET)
    public Single<String> getBalance(@NotNull @PathVariable String accountId) {
        return Single.create(e -> {
            BigDecimal accountBalance = this.getAccountBalance(Long.valueOf(accountId));
            if (!e.isDisposed()) {
                e.onSuccess(String.format("%s $", accountBalance));
            }
        });
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public Single<Deposit> depositFunds(@RequestBody String deposit) {
        return Single.create(e -> {
            Deposit depositedFunds = GSON.fromJson(deposit, Deposit.class);
            if (accountRepository.findAccountByAccountId(depositedFunds.getAccountId()) != null) {
                List<Deposit> depositsToday = depositRepository.getDepositsByTimeStampEquals(Utilities.dateToday());
                if (depositsToday.size() == Constants.MAX_DEPOSIT_TRANSACTIONS_DAILY) {
                    throw new MaxDepositTransactionsReachedException();
                } else if (this.getAccountBalanceToday(depositedFunds.getAccountId()).compareTo(Constants.MAX_DAILY_DEPOSIT) == 0) {
                    throw new MaxDailyDepositException();
                } else if (depositedFunds.getDepositAmout().compareTo(Constants.MAX_DEPOSIT_PER_TRANSACTION) > 0) {
                    throw new MaxDepositPerTransactionException();
                } else {
                    depositRepository.save(depositedFunds);
                    if (!e.isDisposed()) {
                        e.onSuccess(depositedFunds);
                        e.onError(new AccountException());
                    }
                }
            }
            throw new InvalidAccountException();
        });
    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public Single<Withdrawal> withdrawFunds(@RequestBody String withdrawal) {
        return Single.create(e -> {
            Withdrawal withdrawnFunds = GSON.fromJson(withdrawal, Withdrawal.class);
            if (accountRepository.findAccountByAccountId(withdrawnFunds.getAccountId()) != null) {
                List<Withdrawal> withdrawalsToday = withdrawalRepository.getWithdrawalsByTimeStampEquals(Utilities.dateToday());
                if (this.getAccountBalance(withdrawnFunds.getAccountId()).compareTo(withdrawnFunds.getWithdrawalAmount()) < 0) {
                    throw new BalanceLessThanWithdrawalException();
                } else if (withdrawnFunds.getWithdrawalAmount().compareTo(Constants.MAX_DAILY_WITHDRAWAL) > 0) {
                    throw new MaxDailyWithdrawalException();
                } else if (withdrawalsToday.size() == Constants.MAX_WITHDRAWAL_TRANSACTIONS_DAILY) {
                    throw new MaxWithdrawalTransactionsReachedException();
                } else if (withdrawnFunds.getWithdrawalAmount().compareTo(Constants.MAX_WITHDRAWAL_PER_TRANSACTION) > 0) {
                    throw new MaxWithdrawalPerTransactionException();
                } else {
                    withdrawalRepository.save(withdrawnFunds);
                    if (!e.isDisposed()) {
                        e.onSuccess(withdrawnFunds);
                        e.onError(new AccountException());
                    }
                }
            }
            throw new InvalidAccountException();
        });
    }

    private BigDecimal getAccountBalance(Long accountId) {
        // Array Def -> {Deposit, Withdrawal}
        final BigDecimal[] sum = {BigDecimal.valueOf(0), BigDecimal.valueOf(0)};
        depositRepository.getAllByAccountId(accountId).forEach(x -> {
            if (x.getAccountId().equals(accountId)) {
                sum[0] = sum[0].add(x.getDepositAmout());
            }
        });
        withdrawalRepository.getAllByAccountId(accountId).forEach(x -> {
            if (x.getAccountId().equals(accountId)) {
                sum[1] = sum[1].add(x.getWithdrawalAmount());
            }
        });
        return sum[0].subtract(sum[1]);
    }

    private BigDecimal getAccountBalanceToday(Long accountId) {
        // Array Def -> {Deposit, Withdrawal}
        final BigDecimal[] sum = {BigDecimal.valueOf(0), BigDecimal.valueOf(0)};
        depositRepository.getDepositsByTimeStampEquals(Utilities.dateToday()).forEach(x -> {
            if (x.getAccountId().equals(accountId)) {
                sum[0] = sum[0].add(x.getDepositAmout());
            }
        });
        withdrawalRepository.getWithdrawalsByTimeStampEquals(Utilities.dateToday()).forEach(x -> {
            if (x.getAccountId().equals(accountId)) {
                sum[1] = sum[1].add(x.getWithdrawalAmount());
            }
        });
        return sum[0].subtract(sum[1]);
    }
}
