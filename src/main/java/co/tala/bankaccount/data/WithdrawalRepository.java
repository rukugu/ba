package co.tala.bankaccount.data;

import co.tala.bankaccount.models.Withdrawal;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Dennis Mwangi
 */
@Transactional(rollbackOn = Exception.class)
public interface WithdrawalRepository extends JpaRepository<Withdrawal, Long> {

    /**
     * Adds an instance of {@link Withdrawal} to the Database
     *
     * @param withdrawal
     * @return
     */
    @NotNull
    @Override
    Withdrawal save(Withdrawal withdrawal);

    /**
     * Finds and returns all {@link List<Withdrawal>} in the database
     *
     * @param accountId
     * @return {{@link List<Withdrawal>}}
     */

    @NotNull List<Withdrawal> getAllByAccountId(long accountId);

    /**
     * Finds and returns {@link List<Withdrawal>} that occurred on today's date
     *
     * @param today
     * @return {{@link List<Withdrawal>}}
     */
    @NotNull List<Withdrawal> getWithdrawalsByTimeStampEquals(String today);

}