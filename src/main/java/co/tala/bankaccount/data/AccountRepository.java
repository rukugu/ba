package co.tala.bankaccount.data;

import co.tala.bankaccount.models.Account;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Dennis Mwangi
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

    /**
     * Checks if an account exists and if it does, it returns said account
     *
     * @param accountId
     * @return {{@link Account}}
     */
    @NotNull Account findAccountByAccountId(long accountId);

}