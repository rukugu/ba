package co.tala.bankaccount.data;

import co.tala.bankaccount.models.Deposit;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Dennis Mwangi
 */
@Transactional(rollbackOn = Exception.class)
public interface DepositRepository extends JpaRepository<Deposit, Integer> {


    /**
     * Saves an instance of {@link Deposit} to the database
     *
     * @param deposit
     * @return
     */
    @NotNull
    @Override
    Deposit save(Deposit deposit);

    /**
     * Finds and returns all {@link Deposit} form the database
     *
     * @param accountId
     * @return
     */
    @NotNull List<Deposit> getAllByAccountId(long accountId);

    /**
     * Finds and returns {@link List<Deposit>} which occurred today form the database
     *
     * @param today
     * @return
     */
    @NotNull List<Deposit> getDepositsByTimeStampEquals(String today);

}